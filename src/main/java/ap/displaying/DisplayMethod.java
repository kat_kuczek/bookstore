package ap.displaying;

import ap.book.Book;

public interface DisplayMethod {

    String bookDisplay(Book book);
}
