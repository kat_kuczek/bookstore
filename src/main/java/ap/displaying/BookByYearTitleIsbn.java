package ap.displaying;

import ap.book.Book;

public class BookByYearTitleIsbn  implements DisplayMethod {

    @Override
    public String bookDisplay(Book book) {
       return book.getYear() + ", " + book.getTitle() +", " + book.getIsbn();
    }
}
