package ap.displaying;

import ap.book.Book;

public class BookByIsbnTitleYear implements DisplayMethod {

    @Override
    public String bookDisplay(Book book){
        return book.getIsbn() + ", " + book.getTitle() + ", " + book.getYear();
    }
}
