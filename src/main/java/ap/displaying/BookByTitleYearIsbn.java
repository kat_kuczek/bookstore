package ap.displaying;

import ap.book.Book;

public class BookByTitleYearIsbn implements DisplayMethod {

    @Override
    public String bookDisplay(Book book) {
        return book.getTitle() + ", " + book.getYear() + ", " + book.getIsbn();

    }
}
