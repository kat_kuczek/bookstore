package ap.scannerInput;

import ap.category.Category;
import ap.category.CategoryData;

import java.util.Optional;
import java.util.Scanner;

public class CategoryScannerInput {
    private static Scanner scanner = new Scanner(System.in);

    public static Category getCategoryProperitesFromScannerInput() {

        System.out.println("Wpisz ID");
        int id = Integer.parseInt(scanner.nextLine());
        System.out.println("Wpisz nazwę");
        String name = scanner.nextLine();
        System.out.println("Wpisz jaki jest priorytet wyświetlania.");
        int displayPriority = Integer.parseInt(scanner.nextLine());

        Category category = new Category(id, name, displayPriority);

        return category;
    }

    public static Optional<Category> getCetegpryFromScannerInputToChangeItsTitle() {
        Optional<Category> category = null;
        int id = 0;
        while (true) {
            id = CategoryScannerInput.getCategoryID();
            category = CategoryData.getCategoryByID(id);
            if (category.isPresent()) {
                return CategoryData.getCategoryByID(id);
            } else {
                System.out.println("Kategoria o podanym ID nie istnieje.");
            }

        }

    }
    private static int getCategoryID() {
        int id = 0;
        while (true) {
            System.out.println("Podaj ID kategorii którą chcesz zmienić.");
            try {
                id = Integer.parseInt(scanner.nextLine());
            } catch (NumberFormatException e) {
                System.out.println("Niepoprawne dane.");
            }
            return id;
        }
    }
}