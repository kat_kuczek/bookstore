package ap.scannerInput;

import ap.author.Author;
import ap.author.AuthorData;

import java.util.Optional;
import java.util.Scanner;

public class AuthorScannerInput {

static Scanner scanner = new Scanner(System.in);

    public static Author getNewAuthor() {
        System.out.println("Wpisz ID autora.");
        int id = Integer.parseInt(scanner.nextLine());
        System.out.println("Wpisz imię.");
        String firstName = scanner.nextLine();
        System.out.println("Wpisz nazwizko.");
        String secondName = scanner.nextLine();
        System.out.println("Wpisz wiek.");
        int age = Integer.parseInt(scanner.nextLine());

        return new Author(id, firstName, secondName, age);
    }

    public static Optional<Author> getAuthorByID() {
        Optional<Author> author = null;
        while (true) {
            System.out.println("Podaj ID autora.");
            int id = Integer.parseInt(scanner.nextLine());
            author = AuthorData.getAuthorByID(id);
            if (author.isPresent()) {
                return AuthorData.getAuthorByID(id);
            } else {
                System.out.println("Autor o podanym ID nie istnieje.");
            }

        }
    }
}
