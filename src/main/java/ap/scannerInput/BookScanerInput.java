package ap.scannerInput;

import ap.book.Book;
import ap.book.BookData;

import java.util.Optional;
import java.util.Scanner;

public class BookScanerInput {

    public static Optional<Book> getbookByID() {
        Scanner scanner = new Scanner(System.in);
        Optional<Book> book = null;
        while (true) {
            System.out.println("Podaj ID książki.");
            int id = Integer.parseInt(scanner.nextLine());
            book = BookData.getBookByID(id);
            if (book.isPresent()) {
                return BookData.getBookByID(id);
            } else {
                System.out.println("Książka o podanym ID nie istnieje.");
            }
        }
    }
}
