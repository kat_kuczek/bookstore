package ap.category;

public class Category {

    private int id;
    private String name;
    private int displayPriority;

    public Category(int id, String name, int displayPriority) {
        this.id = id;
        this.name = name;
        this.displayPriority = displayPriority;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int getDisplayPriority() {
        return displayPriority;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Category category = (Category) o;

        if (id != category.id) return false;
        if (displayPriority != category.displayPriority) return false;
        return name.equals(category.name);
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + name.hashCode();
        result = 31 * result + displayPriority;
        return result;
    }

    @Override
    public String toString() {
        return "Category{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", displayPriority=" + displayPriority +
                '}';
    }
}
