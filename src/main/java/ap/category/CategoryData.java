package ap.category;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class CategoryData {
    private static CategoryData INSTANCE;
    private static List<Category> list = new ArrayList<>();

    public synchronized static CategoryData getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new CategoryData();
        }
        return INSTANCE;
    }

    private CategoryData() {
    }

    public static Optional<Category> getCategoryByID(int id) {
//        Category category = null;
//        for (Category item : list) {
//            if (item.getId() == id) {
//                category = item;
//            }
//        }
//        return category;
        return CategoryData.getList().stream()
                .filter(item -> item.getId()==id)
                .findFirst();
    }

    public static void setList(List<Category> list) {
        CategoryData.list = list;
    }



    public static List<Category> getList() {
        return list;
    }
}
