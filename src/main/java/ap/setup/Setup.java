package ap.setup;

import java.util.HashMap;
import java.util.Map;

public class Setup {

    private static Map<ConfigurationElement, String> configurationMap = new HashMap<>();

    static {
        configurationMap.put(ConfigurationElement.AURHORFILEPATCH, "authors.csv");
        configurationMap.put(ConfigurationElement.BOOKFILEPATCH, "books.csv");
        configurationMap.put(ConfigurationElement.CATEGORYFILEPATCH, "categories.csv");
    }

    public static String getConfigurationProperty(ConfigurationElement configurationElement) {
        return configurationMap.get(configurationElement);
    }
}
