package ap.author;

import ap.book.Book;
import ap.book.BookData;

import java.util.ArrayList;
import java.util.List;

public class Author {

    private int id;
    private String firstName;
    private String secondName;
    private int age;
    List<Book> bookList = new ArrayList<>();


    public Author(int id, String firstName, String secondName, int age) {
        this.id = id;
        this.firstName = firstName;
        this.secondName = secondName;
        this.age = age;
    }

    public int getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getSecondName() {
        return secondName;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public List<Book> getAuthorsBooks() {
        for(Book item : BookData.getList()) {
            if (BookData.getList().stream().anyMatch(book -> item.getAuthors().contains(this))) {
                bookList.add(item);
            }
        }
        return bookList;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Author author = (Author) o;

        if (id != author.id) return false;
        if (age != author.age) return false;
        if (firstName != null ? !firstName.equals(author.firstName) : author.firstName != null) return false;
        return secondName != null ? secondName.equals(author.secondName) : author.secondName == null;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (firstName != null ? firstName.hashCode() : 0);
        result = 31 * result + (secondName != null ? secondName.hashCode() : 0);
        result = 31 * result + age;
        return result;
    }

    @Override
    public String toString() {
        return "Author{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", secondName='" + secondName + '\'' +
                ", age=" + age +
                '}';
    }
}
