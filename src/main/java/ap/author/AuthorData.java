package ap.author;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class AuthorData {

    private static AuthorData INSTANCE;

    private static List<Author> list = new ArrayList<>();

    public synchronized static AuthorData getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new AuthorData();
        }
        return INSTANCE;
    }

    private AuthorData() {
    }

    public static Optional<Author> getAuthorByID(int id) {
//        Author author = null;
//        for (Author item : list) {
//            if (item.getId() == id) {
//                author = item;
//            }
//        }
//        return author;

        return AuthorData.getList().stream()
                .filter(item -> item.getId()==id)
                .findFirst();
    }

    public static void setList(List<Author> list) {
        AuthorData.list = list;
    }

    public static List<Author> getList() {
        return list;
    }
}
