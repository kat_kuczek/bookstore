package ap.importer;

import ap.author.Author;
import ap.author.AuthorData;
import ap.book.Binding;
import ap.book.Book;
import ap.book.BookData;
import ap.category.CategoryData;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;



public class BookImporter {

    public static List<Book> importBooks(String directory) throws IOException {
        List<Book> list = new ArrayList<Book>();

        try(
                FileReader fileReader = new FileReader(directory);
                BufferedReader bufferedReader = new BufferedReader(fileReader)
        ) {
            String textLine = bufferedReader.readLine();
            while (textLine != null) {
                String[] strings = textLine.split(";");
                String[] authorsArray = strings[5].split(",");

                list.add(new Book(Integer.valueOf(strings[0]), strings[1], Integer.valueOf(strings[2]), Integer.valueOf(strings[3]), Binding.valueOf(strings[4]),
                        getAuthorsOfBook(authorsArray), CategoryData.getCategoryByID(Integer.valueOf(strings[6]))));

                textLine = bufferedReader.readLine();
            }
        }
        BookData.setList(list);
        return list;
    }


    private static List<Author> getAuthorsOfBook(String[] authorsArray){
        List<Author> authorList = new ArrayList<Author>();
        for (String anAuthorsArray : authorsArray) {
            authorList.add(AuthorData.getAuthorByID(Integer.valueOf(anAuthorsArray)).get());
        }
        return authorList;
    }
































//    public static void apacheImportFromFile() {
//        Reader in = new FileReader("path/to/file.csv");
//        Iterable<CSVRecord> records = CSVFormat.RFC4180.parse(in);
//        for (CSVRecord record : records) {
//            int id = Integer.parseInt(record.get(0));
//            String title = record.get(1);
//            String isbn = Integer.parseInt(record.get(2));
//            String
//        }
//    }
}
