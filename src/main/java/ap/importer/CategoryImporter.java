package ap.importer;

import ap.category.Category;
import ap.category.CategoryData;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class CategoryImporter {

    public static List<Category> importCAtegories(String directory) throws IOException {
        List<Category> list = new ArrayList<>();

        try (
                FileReader fileReader = new FileReader(directory);
                BufferedReader bufferedReader = new BufferedReader(fileReader);
        ) {
            String textLine = bufferedReader.readLine();
            while (textLine != null) {
                String[] strings = textLine.split(";");
                list.add(new Category(Integer.valueOf(strings[0]), strings[1], Integer.valueOf(strings[2])));
                textLine = bufferedReader.readLine();
            }
        }

        CategoryData.setList(list);
        return list;
    }
}
