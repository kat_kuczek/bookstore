package ap.importer;

import ap.author.Author;
import ap.author.AuthorData;


import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
public class AuthorImporter {

    public static List<Author> importAuthors(String directory) throws IOException {
        List<Author> list = new ArrayList<>();

        try (
                FileReader fileReader = new FileReader(directory);
                BufferedReader bufferedReader = new BufferedReader(fileReader)
        ) {
            String textLine = bufferedReader.readLine();
            while (textLine != null) {
                String[] strings = textLine.split(";");
                list.add(new Author(Integer.valueOf(strings[0]), strings[1], strings[2], Integer.valueOf(strings[3])));
                textLine = bufferedReader.readLine();
            }
        }
        AuthorData.setList(list);
        return list;
    }


}
