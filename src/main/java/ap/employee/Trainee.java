package ap.employee;

public class Trainee extends Employee{

    private int hours;
    private int salary;

    public Trainee(String name, String country, Gender gender, int hours) {
        super(name, country, gender);
        this.hours = hours;
        this.salary = 50*hours;
    }

    public int getSalary() {
        return salary;
    }

}
