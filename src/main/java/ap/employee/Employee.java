package ap.employee;

public class Employee {
    private String name;
    private String country;
     private Gender gender;
    private int salary;

    public Employee(String name, String country, Gender gender) {
        this.name = name;
        this.country = country;
        this.gender = gender;
    }

    public int getSalary() {
        return salary;
    }
}
