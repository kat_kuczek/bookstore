package ap.employee;

public class Sales extends Employee{

    private int salary = 4000;

    public Sales(String name, String country, Gender gender) {
        super(name, country, gender);
    }

    public int getSalary() {
        return salary;
    }
}
