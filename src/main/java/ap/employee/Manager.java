package ap.employee;

public class Manager extends Employee {
    private int phoneNumber;
    private int salary = 1200;

    public Manager(String name, String country, Gender gender, int phoneNumber) {
        super(name, country, gender);
        this.phoneNumber = phoneNumber;
    }

    public int getSalary() {
        return salary;
    }
}
