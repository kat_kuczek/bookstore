package ap.employee;

public class MainEmployes {
    public static void main(String[] args) {

        Employee sales = new Sales("kowalski", "PL",Gender.MALE);
        System.out.println(sales.getSalary());
        Employee manager = new Manager("torer", "PL" , Gender.FEMALE,212121);
        System.out.println(manager.getSalary());
        Employee trainee = new Trainee("mil", "PL", Gender.MALE, 20);
        System.out.println(trainee.getSalary());
    }

}
