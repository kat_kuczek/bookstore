package ap.book;

import ap.book.Book;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class BookFunction {

    Comparator<Book> bookComparator = new Comparator<Book>() {
        @Override
        public int compare(Book o1, Book o2) {
            return o1.getYear() - o2.getYear();
        }
    };

    public Book findBookByIsbn(List<Book> bookList, int isbn) {
//        ap.book.Book ap = null;
//        for (ap.book.Book item : bookList) {
//            if (item.getIsbn() == isbn) {
//                ap = item;
//            }
//        }
//
//
//        if (ap == null) {
//            throw new NoSuchElementException("No value present");
//        }else {
//            return ap;
//        }


// STREAMEM
        return bookList.stream()
                .filter(item -> item.getIsbn() == isbn).findFirst().get();
    }

    public Optional<Book> findBookByIsbnSTREAM(List<Book> bookList, int isbn) {
// STREAMEM
        return bookList.stream()
                .filter(item -> item.getIsbn() == isbn).findFirst();
    }


    public int sumOfPublicationsYears(List<Book> bookList) {
//        int sum = 0;
//        for (ap.book.Book item : bookList) {
//            sum += item.getYear();
//        }
//        return sum;

        return bookList.stream()
                .collect(Collectors.summingInt(Book::getYear));
    }

    public int booksPublishedAfter2007(List<Book> bookList) {
//        int sum = 0;
//        for (ap.book.Book item : bookList) {
//            if (item.getYear() > 2007) {
//                sum++;
//            }
//        }
//        return sum;

        return (int) bookList.stream()
                .filter(item -> item.getYear() > 2007)
                .count();
    }

    public boolean checkIfAllBooksArePublishedAfter2000(List<Book> bookList) {
//        for (ap.book.Book item : bookList) {
//            if (item.getYear() <= 2000) {
//                return false;
//            }
//        }
//        return true;


//STREAMEM

        return bookList.stream().allMatch(item -> item.getYear() > 2000);

    }

    public List<Book> twoLastBooks(List<Book> bookList) {

        return bookList.subList(bookList.size() - 2, bookList.size());

    }

    public List<Book> earliestPublished(List<Book> bookList) {


// List<ap.book.Book> books = new ArrayList<>();
//        for (ap.book.Book item : bookList) {
//            if (item.getYear() == bookList.get(0).getYear()) {
//                books.add(item);
//            }
//        }
// return books;

//STREAMEM
        bookList.sort(bookComparator);
        return bookList.stream()
                .filter(item -> item.getYear() == bookList.get(0).getYear())
                .collect(Collectors.toList());
    }

    public List<Book> latestPublished(List<Book> bookList) {

        bookList.sort(bookComparator.reversed());
//        for (ap.book.Book item : bookList) {
//            if (item.getYear() == bookList.get(0).getYear()) {
//                list.add(item);
//            }
//        return books;

        List<Book> list = bookList.stream()
                .filter(item -> item.getYear() == bookList.get(0).getYear())
                .collect(Collectors.toList());
        return list;
    }
//    8.  Zwróć średni rok wydania książki.
//

    public double publishAverage(List<Book> bookList) {

        double average = 0;
        for (Book item : bookList) {
            average = average + item.getYear();
        }
        return average / (bookList.size());

//        double average = bookList.stream()
//                .collect(Collectors.averagingDouble(ap.book.Book::getYear));
//        return average;
    }

    //    9.  Zwróć informacje o tym czy jakakolwiek książka w naszym katalogu jest wydana przed  2003 rokiem.
//
    public boolean isAnypublishedBefore2003(List<Book> bookList) {

//        for (ap.book.Book item : bookList) {
//            if (item.getYear() < 2003) {
//                return true;
//            }
//        }
//     return false;

//STREAMEM
        return bookList.stream().anyMatch(item -> item.getYear() < 2003);

        //     }

    }
//    10. Zwróć wszystkie książki, których tytuł zaczyna się od litery “C” i były one wydane po 2010 roku.

    public List<Book> booksStartsFromCPublishedAfter2010(List<Book> bookList) {

        List<Book> books = new ArrayList<>();
        for (Book item : bookList) {
            if (item.getTitle().startsWith("C") && item.getYear() > 2010) {
                books.add(item);
            }
        }
//        List<ap.book.Book> books = bookList.stream()
//                .filter(item -> item.getTitle().startsWith("C"))
//                .filter(item -> item.getYear() > 2010)
//                .collect(Collectors.toList());
//
        return books;
    }
//    11. Posortuj książki po roku wydania zaczynajac od wydanej najwcześniej.
//

    public List<Book> booksSortedFromTheOldest(List<Book> bookList) {

        bookList.sort(bookComparator);
        return bookList;

        //       List<ap.book.Book> books = bookList.stream()
//                .sorted(bookComparator)
//                .collect(Collectors.toList());
//        return books;


    }

    //    12. Posortuj książki po roku wydania zaczynajac od wydanej najpóźniej.
    public List<Book> booksSortedFromTheNewest(List<Book> bookList) {

        bookList.sort(bookComparator.reversed());
        return bookList;

//        List<ap.book.Book> books = bookList.stream()
//                .sorted(bookComparator.reversed())
//                .collect(Collectors.toList());
//        return books;
    }

    //    1.  Dodajmy 100 lat do roku wydania każdej książki.
//
    public List<Book> Yeears100AddedToEacheBook(List<Book> bookList) {

//        for (ap.book.Book item : bookList) {
//            item.setYear(item.getYear() + 100);
//        }
//        return bookList;

        //STREAMAMI
        bookList.stream()
                .forEach(item -> item.setYear(item.getYear() + 100));

        return bookList;
    }


//            2.  Zwróć tytuły wszystkich książek, których rok jest podzielny przez 2.

    public List<String> booksPublishYearDivisibleby2(List<Book> bookList) {

//        List<String> stringList = new ArrayList<>();

//        for (ap.book.Book item : bookList) {
//           if(item.getYear()%2==0){
//               stringList.add(item.getTitle());
//           }
//        }
//        return stringList;

        //STREAMAMI
        return bookList.stream()
                .filter(item -> item.getYear() % 2 == 0)
                .map(Book::getTitle)
                .collect(Collectors.toList());
    }


//            3.  Posortuj książki alfabetycznie po tytule.

    public List<Book> sortByTitle(List<Book> bookList) {
     bookList.sort(new Comparator<Book>() {
         @Override
         public int compare(Book o1, Book o2) {
             return o1.getTitle().compareTo(o2.getTitle());
         }
     });

     return bookList;
    }
}
