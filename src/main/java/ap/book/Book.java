package ap.book;

import ap.author.Author;
import ap.category.Category;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class Book {
    private int id;
    private String title;
    private int isbn;
    private int year;
    private Binding binding;
    private List<Author> authors;
    private Optional<Category> category;


    public Book(int id, String title, int isbn, int year, Binding binding, List<Author> authors, Optional<Category> category) {
        this.id = id;
        this.title = title;
        this.isbn = isbn;
        this.year = year;
        this.binding = binding;
        this.authors = authors;
        this.category = category;
    }

    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public int getIsbn() {
        return isbn;
    }

    public int getYear() {
        return year;
    }

    public Optional<Category> getCategory() {
        return category;
    }

    public List<Author> getAuthors() {
        return authors;
    }

    public Binding getBinding() {
        return binding;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<Integer> getAuthorsById() {
        return authors.stream().map(Author::getId).collect(Collectors.toList());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Book book = (Book) o;

        if (id != book.id) return false;
        if (isbn != book.isbn) return false;
        if (year != book.year) return false;
        if (title != null ? !title.equals(book.title) : book.title != null) return false;
        if (binding != book.binding) return false;
        if (authors != null ? !authors.equals(book.authors) : book.authors != null) return false;
        return category != null ? category.equals(book.category) : book.category == null;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (title != null ? title.hashCode() : 0);
        result = 31 * result + isbn;
        result = 31 * result + year;
        result = 31 * result + (binding != null ? binding.hashCode() : 0);
        result = 31 * result + (authors != null ? authors.hashCode() : 0);
        result = 31 * result + (category != null ? category.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Book{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", isbn=" + isbn +
                ", year=" + year +
                ", binding=" + binding +
                ", authors=" + authors +
                ", category=" + category +
                "}";
    }
}


