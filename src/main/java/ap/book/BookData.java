package ap.book;

import ap.author.Author;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class BookData {

    private static BookData INSTANCE;
    private static List<Book> list = new ArrayList<Book>();

    public synchronized static BookData getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new BookData();
        }
        return INSTANCE;
    }

    private BookData() {
    }

    public static void setList(List<Book> list) {
        BookData.list = list;
    }

    public static List<Book> getList() {
        return list;
    }

    public static Optional<Book> getBookByID(int id) {
        Book book = null;
//        for (Book item : list) {
//            if (item.getId() == id) {
//                book = item;
//            }
//        }
//        return book;

        return BookData.getList().stream()
                .filter(item -> item.getId()==id)
                .findFirst();
    }
    public static List<Book> getAuthorsBooks(Author author){
        return list.stream().filter(book -> book.getAuthors().contains(author)).collect(Collectors.toList());
    }
}

