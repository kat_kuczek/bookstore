package ap.book;

import ap.displaying.DisplayMethod;

public class BookDisplayMethod {
    private static DisplayMethod displayMethod;


    public static void setDisplayMethod(DisplayMethod displayMethod) {
        BookDisplayMethod.displayMethod = displayMethod;
    }

    public static String display(Book book) {
        if (displayMethod == null) {
          return book.toString();
        }
        return displayMethod.bookDisplay(book);
    }

}
