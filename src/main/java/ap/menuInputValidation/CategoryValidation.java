package ap.menuInputValidation;

import ap.category.Category;
import ap.category.CategoryData;
import org.apache.commons.lang3.StringUtils;

public class CategoryValidation {

    public static boolean isCategoryIdValid(Category category) {
        return CategoryData.getList().stream().noneMatch(item -> item.getId() == category.getId());
    }

    public static boolean isCategoryNameValid(Category category) {
        return StringUtils.isAlpha(category.getName());
    }

    public static boolean isDisplayPriorityValid(Category category) {
        return category.getDisplayPriority() > 0;
    }
}
