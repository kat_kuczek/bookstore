package ap.menuInputValidation;

import ap.author.Author;
import ap.author.AuthorData;
import org.apache.commons.lang3.StringUtils;

public class AuthorValidation {

     public static boolean isAuthorsIdValid(Author author) {
        return AuthorData.getList().stream().noneMatch(item -> item.getId() == author.getId());
    }

     public static boolean isAuthorsNameValid(Author author) {
        return StringUtils.isAlpha(author.getFirstName()) && StringUtils.isAlpha(author.getSecondName());
    }

     public static boolean isAuthorsAgeValid(Author author) {
        return author.getAge() >0;
    }

}
