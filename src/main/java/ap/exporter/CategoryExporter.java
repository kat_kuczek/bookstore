package ap.exporter;

import ap.setup.ConfigurationElement;
import ap.setup.Setup;
import ap.category.Category;
import ap.category.CategoryData;

import java.io.*;

public class CategoryExporter {

    public static void saveCategoriesList() throws IOException {
        File file = new File(Setup.getConfigurationProperty(ConfigurationElement.CATEGORYFILEPATCH));

        try (
                PrintWriter pw = new PrintWriter(new FileOutputStream(file, false));
        ) {
            for (Category item : CategoryData.getList()) {

                pw.write(item.getId() + ";" + item.getName()+ ";" + item.getDisplayPriority() +"\n");
            }

        }
    }
}
