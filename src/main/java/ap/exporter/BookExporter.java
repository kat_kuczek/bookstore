package ap.exporter;

import ap.setup.ConfigurationElement;
import ap.setup.Setup;
import ap.book.Book;
import ap.book.BookData;

import java.io.*;
import java.util.stream.Collectors;

public class BookExporter {

    public static void saveBookList() throws IOException {
        File file = new File(Setup.getConfigurationProperty(ConfigurationElement.BOOKFILEPATCH));

        try (
                PrintWriter pw = new PrintWriter(new FileOutputStream(file, false))
        ) {
            for (Book item : BookData.getList()) {

                    pw.write(item.getId() + ";"
                        + item.getTitle() + ";"
                        + item.getIsbn() + ";"
                        + item.getYear() + ";"
                        + item.getBinding() + ";"
                        + item.getAuthorsById().stream().map(String::valueOf).collect(Collectors.joining(",")) + ";"
                        + item.getCategory().get().getId()
                        + "\n");
            }


        }
    }
}
