package ap.exporter;

import ap.setup.ConfigurationElement;
import ap.setup.Setup;
import ap.author.Author;
import ap.author.AuthorData;

import java.io.*;

public class AuthorExporter {


    public static void saveAuthorsList() throws IOException {
        File file = new File(Setup.getConfigurationProperty(ConfigurationElement.AURHORFILEPATCH));

        try (
                PrintWriter pw = new PrintWriter(new FileOutputStream(file, false))
        ) {
            for (Author item : AuthorData.getList()) {
                pw.write( item.getId() + ";" + item.getFirstName() + ";" + item.getSecondName() + ";" + item.getAge() + "\n");
            }


        }
    }
}
