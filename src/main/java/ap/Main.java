package ap;

import ap.book.Book;
import ap.displaying.BookByYearTitleIsbn;
import ap.displaying.DisplayMethod;
import ap.exporter.AuthorExporter;
import ap.menu.Menu;
import ap.order.Binding;
import org.apache.log4j.Logger;

import java.io.FileNotFoundException;
import java.io.IOException;

public class Main {
    static Logger log = Logger.getLogger(AuthorExporter.class.getName());

    public static void main(String[] args) {

//        ap.order.Binding binding = new Binding() {
//            @Override
//            public void printingRequest() {
//
//            }
//
//            @Override
//            public void sendEmailtoBookstore() {
//
//            }
//        };
//
//        DisplayMethod displayMethod = new BookByYearTitleIsbn();



         try {
            Menu.run();
        } catch (
                FileNotFoundException e) {
            System.out.println("Zła ścieżka.");
             log.error(e.getMessage());

         } catch (
                IOException e) {
             log.error(e.getMessage());
            System.out.println("Niepoprawna zawartość pliku.");
        }
//        catch (Exception r){
//             log.error(r.getMessage(), r);
//       }

    }

}
