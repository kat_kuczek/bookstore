package ap.menu;

import ap.book.Book;
import ap.book.BookData;
import ap.book.BookDisplayMethod;
import ap.order.Binding;
import ap.order.HardCover;
import ap.order.PaperBack;
import ap.scannerInput.BookScanerInput;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.Scanner;
import java.util.stream.Collectors;

public class MenuBook {

    static Scanner scanner = new Scanner(System.in);

    public static void printAllBooks() {
        for (Book item : BookData.getList()) {
            System.out.println(BookDisplayMethod.display(item));
        }
    }

    public static void printFoundBookByIsbn() {
        int isbn = scanner.nextInt();
        scanner.nextLine();
        Book book = BookData.getList().stream()
                .filter(item -> item.getIsbn() == isbn).findFirst().get();
        System.out.println(BookDisplayMethod.display(book));
    }

    public static void printSortedByTitle() {
        BookData.getList().sort(Comparator.comparing(Book::getTitle));

        for (Book item : BookData.getList()) {
            System.out.println(BookDisplayMethod.display(item));
        }
    }

    public static void printBooksFromWzorceProjektoweCategory() {
        List<Book> bookList = BookData.getList().stream()
                .filter(item -> item.getCategory().get().getName().equals("Wzorce projektowe"))
                .collect(Collectors.toList());

        for (Book item : bookList) {
            System.out.println(BookDisplayMethod.display(item));
        }
    }

    public static void changeBooksTitle() {
        Optional<Book> book = BookScanerInput.getbookByID();
        System.out.println("Podaj nowy tytuł.");
        String newTitle = scanner.nextLine();

        book.get().setTitle(newTitle);
    }



    public static void deleteBook() {
        Optional<Book> book = null;

        while (true) {
            System.out.println("Podaj ID książki którą chcesz usunąć.");
            int id = Integer.parseInt(scanner.nextLine());
            book = BookData.getBookByID(id);
            if (book.isPresent()) {
                BookData.getList().remove(book.get());
                break;
            } else {
                System.out.println("Książka o podanym ID nie istnieje.");
            }
        }

    }
//TODO porpaweic
    public static void order(){
        Scanner scanner = new Scanner(System.in);
        boolean looping = true;
        Optional<Book> book = BookScanerInput.getbookByID();

        while (looping) {
            System.out.println("Wybierz rodzaj oprawy którą chcesz dodrukować.\nM - miękka, T - twarda.");
            String binding = scanner.nextLine();
            if (binding.equals("T")) {
                Binding.binding = new HardCover();
            } else if (binding.equals("M")) {
                Binding.binding = new PaperBack();
            } else {
                System.out.println("Wpisujesz źle.");
                continue;
            }

            System.out.println("Rozpoczynam zlecanie do druku książkę o tytule: "  + book.get().getTitle());

            Binding.binding.printingRequest();

            System.out.println("Zakończono zlecanie do druku.");

            Binding.binding.sendEmailtoBookstore();

            looping = false;
        }
    }
}
