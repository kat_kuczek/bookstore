package ap.menu;

import ap.book.BookDisplayMethod;
import ap.displaying.BookByIsbnTitleYear;
import ap.displaying.BookByTitleYearIsbn;
import ap.displaying.BookByYearTitleIsbn;
import ap.exporter.AuthorExporter;
import ap.exporter.BookExporter;
import ap.exporter.CategoryExporter;
import ap.importer.AuthorImporter;
import ap.importer.BookImporter;
import ap.importer.CategoryImporter;
import ap.setup.ConfigurationElement;
import ap.setup.Setup;

import java.io.IOException;
import java.util.Scanner;

public class Menu {
    private static boolean loopingSubj = true;

    //TODO runnable co to jest
    public static void run() throws IOException {
        Menu.imports();
        Menu.menu();
    }

    private static void imports() throws IOException {
        AuthorImporter.importAuthors(Setup.getConfigurationProperty(ConfigurationElement.AURHORFILEPATCH));
        CategoryImporter.importCAtegories(Setup.getConfigurationProperty(ConfigurationElement.CATEGORYFILEPATCH));
        BookImporter.importBooks(Setup.getConfigurationProperty(ConfigurationElement.BOOKFILEPATCH));
    }

    private static void exports() throws IOException {
        BookExporter.saveBookList();
        CategoryExporter.saveCategoriesList();
        AuthorExporter.saveAuthorsList();
    }

    public static void menu() throws IOException {
        Scanner scanner = new Scanner(System.in);
        int menu;

        while (loopingSubj) {
            printMenuOptions();
            String input = scanner.nextLine();
            try {
                menu = Integer.parseInt(input);
            } catch (NumberFormatException e) {
                System.out.println("Wpisz odpowiednią cyfrę.");
                continue;
            }
            choseMenuOptions(menu);
        }
    }


    private static void printMenuOptions() {
        System.out.println(
                "____________________________________\n" +
                        "Wybierz: \n" +
                        "1 aby wyświtlić email do księgarni,\n" +
                        "2 aby wyświetlić wszystkie książki,\n" +
                        "3 aby wyświetlić wszystkich autorów,\n" +
                        "4 aby wyswielić wszystkie karegorie.\n" +
                        "5 aby znależć książkę po ISBN. \n" +
                        "6 aby wyświetlić wszystkie ksiązki posortowane po tytule. \n" +
                        "7 aby wyświetlić wszystkie książki z kategorii \"Wzorce projektowe.\n" +
                        "8 aby dodać autora. \n" +
                        "9 aby dodać kategorię. \n" +
                        "10 aby zmienić tytuł ksiązki. \n" +
                        "11 aby wyświetlać ksiązki po rok, tytuł, ISBN. \n" +
                        "12 aby wyświetlić książki po tytuł, rok, ISBN. \n" +
                        "13 aby wyświetlić ksiązki po Isbn, tytuł, rok. \n" +
                        "14 aby zapisać listę autorów. \n" +
                        "15 aby zapisać listę kategprii. \n" +
                        "16 aby zmienić wiek autora. \n" +
                        "17 aby wyswietlić wszystkie książki podanego autora. \n" +
                        "18 aby wyswietlić wsyzstkich autorów i książki które napisali \n" +
                        "19 aby wyświetlić nazwiska autorów i ilośc wydanych książek. \n" +
                        "20 aby zapisać listę książek. \n" +
                        "21 aby zmienić nazwe kategorii. \n" +
                        "22 aby zapisać stan. \n" +
                        "23 aby usunac ksiazke. \n" +
                        "24 aby zamówić dodruk nowej ksiązki.\n" +
                        "25 aby zamknać aplikację,\n" +
                        "____________________________________");
    }

    private static void choseMenuOptions(int menu) throws IOException {
        switch (menu) {
            case 1:
                System.out.println("Email to księgarni: maildoksiegarni@ksiegarnia.pl");
                break;
            case 2:
                System.out.println("Dostęne książki: \n");
                MenuBook.printAllBooks();
                break;
            case 3:
                System.out.println("Dostępni autorzy: \n");
                MenuAuthor.printAllAuthors();
                break;
            case 4:
                System.out.println("Dostęne kategorie: \n");
                MenuCategory.printAllCategories();
                break;
            case 5:
                System.out.println("Podaj ISBN.");
                MenuBook.printFoundBookByIsbn();
                break;
            case 6:
                MenuBook.printSortedByTitle();
                break;
            case 7:
                MenuBook.printBooksFromWzorceProjektoweCategory();
                break;
            case 8:
                MenuAuthor.addAuthor();
                System.out.println("Autor dodany.");
                break;
            case 9:
                MenuCategory.addCategory();
                System.out.println("Kategoria dodana.");
                break;
            case 10:
                MenuBook.changeBooksTitle();
                System.out.println("Zmieniono tytuł książki.");
                break;
            case 11:
                BookDisplayMethod.setDisplayMethod(new BookByYearTitleIsbn());
                System.out.println("Zmienieno wyswietlanie książki na rok, tytuł, ISBN.");
                break;
            case 12:
                BookDisplayMethod.setDisplayMethod(new BookByTitleYearIsbn());
                System.out.println("Zmienieno wyswietlanie książki na tytuł, rok, ISBN.");
                break;
            case 13:
                BookDisplayMethod.setDisplayMethod(new BookByIsbnTitleYear());
                System.out.println("Zmienieno wyswietlanie książki na Isbn, tytuł, rok.");

                break;
            case 14:
                AuthorExporter.saveAuthorsList();
                System.out.println("Zapisano.");
                break;
            case 15:
                CategoryExporter.saveCategoriesList();
                System.out.println("Zapisano.");
                break;
            case 16:
                MenuAuthor.changeAutorsAge();
                System.out.println("Zmienino wiek autora.");
                break;
            case 17:
                MenuAuthor.printAllBooksOfChoosenAuthor();

                break;
            case 18:
                MenuAuthor.printAllAuthorsAndTheirsBooks();
                break;
            case 19:
                MenuAuthor.printAllAuthorsSurnameAndQuantintyOfBooks();
                break;
            case 20:
                BookExporter.saveBookList();
                System.out.println("Zapisano.");
                break;
            case 21:
                MenuCategory.changeCategoryName();
                System.out.println("Zmieniono nazwę kategorii");
                break;
            case 22:
                Menu.exports();
                System.out.println("Zapisano.");
                break;
            case 23:
                MenuBook.deleteBook();
                System.out.println("Książka została usunięta.");
                break;
            case 24:
                MenuBook.order();
                break;
            case 25:
                System.out.println("Zamykam aplikację.");
                loopingSubj = false;
                break;
        }
    }
}
