package ap.menu;

import ap.author.Author;
import ap.author.AuthorData;
import ap.book.Book;
import ap.book.BookData;
import ap.book.BookDisplayMethod;
import ap.menuInputValidation.AuthorValidation;
import ap.scannerInput.AuthorScannerInput;

import java.awt.event.KeyEvent;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

public class MenuAuthor {

    private static Scanner scanner = new Scanner(System.in);

    public static void printAllAuthors() {
        for (Author item : AuthorData.getList()) {
            System.out.println(item);
        }
    }

    public static void addAuthor( ) {
        boolean loopingSubj = true;
        //    KeyEvent keyEvent;

        while (loopingSubj) {
            try {
                Author author = AuthorScannerInput.getNewAuthor();
                if (isAuthorValid(author)) {
                    AuthorData.getList().add(author);
                    loopingSubj = false;
                }
            } catch (NumberFormatException n) {
                System.out.println("Niepoprawne dane.");
            }
        }
    }

    private static boolean isAuthorValid(Author author) {
        ArrayList<String> comunicatesList = new ArrayList<>();
        boolean valid = true;

        if (!AuthorValidation.isAuthorsIdValid(author)) {
            comunicatesList.add("To ID już istnieje.");
            valid = false;
        }
        if (!AuthorValidation.isAuthorsAgeValid(author)) {
            comunicatesList.add("Wpisujesz wiek niepoprawnie.");
            valid = false;
        }
        if (!AuthorValidation.isAuthorsNameValid(author)) {
            comunicatesList.add("Wpisujesz imię i nazwisko niepoprawnie.");
            valid = false;
        }
        if (!valid) {
            comunicatesList
                    .forEach(System.out::println);
            System.out.println("Spróbuj jeszcze raz");
        }
        return valid;
    }

    public static void changeAutorsAge() {
        Optional<Author> author = AuthorScannerInput.getAuthorByID();
        System.out.println("Podaj aktualny wiek.");
        int age = 0;
        try {
            age = Integer.parseInt(scanner.nextLine());
        } catch (NumberFormatException e) {
            System.out.println("Niepoprawnie wpisujesz wiek.");
        }
        author.get().setAge(age);
    }


    public static void printAllBooksOfChoosenAuthor() {
        Author author = AuthorScannerInput.getAuthorByID().get();
        BookData.getList().stream()
                .filter(item -> item.getAuthors().contains(author))
                .forEach(item -> System.out.println(BookDisplayMethod.display(item)));

    }

    public static void printAllAuthorsAndTheirsBooks() {
        //tworzenie mapy klucz -> autor, value -> lista jego ksiazek
        Map<Author, List<Book>> map = AuthorData.getList().stream()
                .collect(Collectors.toMap(Function.identity(), Author::getAuthorsBooks));
        //wyswietlanie
        map.entrySet().stream().forEach(item -> {
            System.out.print(item.getKey() + " ");
            item.getValue().stream()
                    .forEach(it -> System.out.print(BookDisplayMethod.display(it)));
            System.out.println();
        });
    }

    public static void printAllAuthorsSurnameAndQuantintyOfBooks() {
        Map<Author, Integer> map = AuthorData.getList().stream()
                .collect(Collectors.toMap(Function.identity(), author -> author.getAuthorsBooks().size()));

        map.entrySet().stream()
                .forEach(item -> System.out.println(item.getKey() + ": " + item.getValue()));
    }

}


