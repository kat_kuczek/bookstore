package ap.menu;

import ap.category.Category;
import ap.category.CategoryData;
import ap.menuInputValidation.CategoryValidation;
import ap.scannerInput.CategoryScannerInput;

import java.util.ArrayList;
import java.util.Optional;
import java.util.Scanner;

public class MenuCategory {

    static Scanner scanner = new Scanner(System.in);

    public static void printAllCategories() {
        for (Category item : CategoryData.getList()) {
            System.out.println(item);
        }
    }

    public static void addCategory() {
        boolean loopingSubj = true;

        while (loopingSubj) {
            try {
                Category category = CategoryScannerInput.getCategoryProperitesFromScannerInput();
                if (isCategoryValid(category)) {
                    CategoryData.getList().add(category);
                    loopingSubj = false;
                }
            } catch (NumberFormatException n) {
                System.out.println("Niepoprawne dane.");
            }
        }

    }

    private static boolean isCategoryValid(Category category) {
        ArrayList<String> comunicatesList = new ArrayList<>();
        boolean valid = true;

        if (!CategoryValidation.isCategoryIdValid(category)) {
            comunicatesList.add("To ID już istnieje.");
            valid = false;
        }
        if (!CategoryValidation.isCategoryNameValid(category)) {
            comunicatesList.add("Nieporawnie wpisujesz nazwę.");
            valid = false;
        }
        if (!CategoryValidation.isDisplayPriorityValid(category)) {
            comunicatesList.add("Wpisana kategoria jest niepoprawna.");
            valid = false;
        }
        if (!valid) {
            comunicatesList
                    .forEach(System.out::println);
            System.out.println("Spróbuj jeszcze raz");
        }
        return valid;
    }

    public static void changeCategoryName() {
        Optional<Category> category = CategoryScannerInput.getCetegpryFromScannerInputToChangeItsTitle();
        System.out.println("Podaj nowy tytuł.");
        String newTitle = scanner.nextLine();
        category.get().setName(newTitle);
    }

    ////TODO optional

}
