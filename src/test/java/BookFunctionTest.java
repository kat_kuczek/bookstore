import ap.book.Binding;
import ap.book.Book;
import ap.book.BookFunction;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class BookFunctionTest {
    List<Book> bookList = new ArrayList<>();
    BookFunction bookFunction = new BookFunction();

    @Before
    public void initList() {
        System.out.println("Buildiing fresh BookList.");
        bookList.add((new Book(1, "Clean Code", 132350882, 2011, Binding.T, null, null)));
        bookList.add(new Book(2, "Effective Java (3rd Edition)", 134685997, 2018, Binding.M, null, null));
        bookList.add(new Book(3, "Test Driven Development: By Example", 321146530, 2002, Binding.M, null, null));
        bookList.add(new Book(4, "Patterns of Enterprise Application Architecture", 321127420, 2002, Binding.T, null, null));
        bookList.add(new Book(5, "Head First Design Patterns", 596007124, 2004, Binding.M, null, null));
    }

    @Test
    public void findBookByIsbnTest() {

        Book actualBook = bookFunction.findBookByIsbn(bookList, 321146530);
        Assertions.assertEquals(actualBook.getIsbn(), 321146530);
        //TODO reszte tak samo
    }

//TEST na neipoprawny ISBN

    @Test
    public void sumOfPublicationsYearsTest() {

        int actualSum = bookFunction.sumOfPublicationsYears(bookList);
        int expectedSum = 10037;
        Assertions.assertEquals(actualSum, expectedSum);
    }

    @Test
    public void booksPublishedAfter2007Test() {

        int actualCount = bookFunction.booksPublishedAfter2007(bookList);
        int expectedCount = 2;
        Assertions.assertEquals(actualCount, expectedCount);
    }

    @Test
    public void checkIfAllBooksArePublishedAfter2000Test() {

        boolean actual = bookFunction.checkIfAllBooksArePublishedAfter2000(bookList);
        Assertions.assertTrue(actual);
    }

    @Test
    public void twoLastBooksTest() {

        List<Book> actual = bookFunction.twoLastBooks(bookList);
        List<Book> expected = new ArrayList<Book>();
        expected.add(new Book(4, "Patterns of Enterprise Application Architecture", 321127420, 2002, Binding.T, null, null));
        expected.add(new Book(5, "Head First Design Patterns", 596007124, 2004, Binding.M, null, null));

        Assertions.assertEquals(expected,actual);
    }

    @Test
    public void earliestPublishedTest() {

        List<Book> actual = bookFunction.earliestPublished(bookList);
        List<Book> expected = new ArrayList<>();
        expected.add(new Book(3, "Test Driven Development: By Example", 321146530, 2002, Binding.M, null, null));
        expected.add(new Book(4, "Patterns of Enterprise Application Architecture", 321127420, 2002, Binding.T, null, null));

        Assertions.assertEquals(actual, expected);
    }

    @Test
    public void latestPublishedTest() {

        List<Book> actual = bookFunction.latestPublished(bookList);
        List<Book> expected = new ArrayList<>();
        expected.add(new Book(2, "Effective Java (3rd Edition)", 134685997, 2018, Binding.M, null, null));

        Assertions.assertEquals(actual, expected);
    }

    @Test
    public void publishAverageTest() {

        double actual = bookFunction.publishAverage(bookList);
        double expected = 2007.4;
        Assertions.assertEquals(expected, actual);
    }

    @Test
    public void isAnypublishedBefore2003() {

        boolean actual = bookFunction.isAnypublishedBefore2003(bookList);

        Assertions.assertTrue(actual);
    }

    @Test
    public void booksStartsFromCPublishedAfter2010() {

        List<Book> actual = bookFunction.booksStartsFromCPublishedAfter2010(bookList);
        List<Book> expected = new ArrayList<>();
        expected.add(new Book(1, "Clean Code", 132350882, 2011, Binding.T, null, null));
        Assertions.assertEquals(expected, actual);
    }

    @Test
    public void booksSortedFromTheOldest() {

        List<Book> actual = bookFunction.booksSortedFromTheOldest(bookList);
        List<Book> expected = new ArrayList<>();
        expected.add(new Book(3, "Test Driven Development: By Example", 321146530, 2002, Binding.M, null, null));
        expected.add(new Book(4, "Patterns of Enterprise Application Architecture", 321127420, 2002, Binding.T, null, null));
        expected.add(new Book(5, "Head First Design Patterns", 596007124, 2004, Binding.M, null, null));
        expected.add((new Book(1, "Clean Code", 132350882, 2011, Binding.T, null, null)));
        expected.add(new Book(2, "Effective Java (3rd Edition)", 134685997, 2018, Binding.M, null, null));

        Assertions.assertEquals(expected, actual);
    }

    @Test
    public void booksSortedFromTheNewest() {

        List<Book> expected = new ArrayList<>();
        List<Book> actual = bookFunction.booksSortedFromTheNewest(bookList);
        expected.add(new Book(2, "Effective Java (3rd Edition)", 134685997, 2018, Binding.M, null, null));
        expected.add((new Book(1, "Clean Code", 132350882, 2011, Binding.T, null, null)));
        expected.add(new Book(5, "Head First Design Patterns", 596007124, 2004, Binding.M, null, null));
        expected.add(new Book(3, "Test Driven Development: By Example", 321146530, 2002, Binding.M, null, null));
        expected.add(new Book(4, "Patterns of Enterprise Application Architecture", 321127420, 2002, Binding.T, null, null));

        Assertions.assertEquals(expected, actual);
    }


    @Test
    public void findBookByIsbnSTREAM() {

        Optional<Book> actualBook = bookFunction.findBookByIsbnSTREAM(bookList, 30);

        Assertions.assertEquals(actualBook.isPresent(), false);

    }

    @Test
    public void yeears100AddedToEacheBook() {

        List<Book> actual = bookFunction.Yeears100AddedToEacheBook(bookList);
        List<Book> expected = new ArrayList<>();
        expected.add((new Book(1, "Clean Code", 132350882, 2111, Binding.T, null, null)));
        expected.add(new Book(2, "Effective Java (3rd Edition)", 134685997, 2118, Binding.M, null, null));
        expected.add(new Book(3, "Test Driven Development: By Example", 321146530, 2102, Binding.M, null, null));
        expected.add(new Book(4, "Patterns of Enterprise Application Architecture", 321127420, 2102, Binding.T, null, null));
        expected.add(new Book(5, "Head First Design Patterns", 596007124, 2104, Binding.M, null, null));

        Assertions.assertEquals(expected,actual);
    }

    @Test
    public void booksPublishYearDivisibleby2() {

        List<String> actual = bookFunction.booksPublishYearDivisibleby2(bookList);
        List<String> expected = new ArrayList<>();
        expected.add("Effective Java (3rd Edition)");
        expected.add("Test Driven Development: By Example");
        expected.add("Patterns of Enterprise Application Architecture");
        expected.add("Head First Design Patterns");

        Assertions.assertEquals(expected,actual);
    }

    @Test
    public void sortByTitle() {
        List<Book> actual = bookFunction.sortByTitle(bookList);
        List<Book> expected = new ArrayList<>();
        expected.add((new Book(1, "Clean Code", 132350882, 2011, Binding.T, null, null)));
        expected.add(new Book(2, "Effective Java (3rd Edition)", 134685997, 2018, Binding.M, null, null));
        expected.add(new Book(5, "Head First Design Patterns", 596007124, 2004, Binding.M, null, null));
        expected.add(new Book(4, "Patterns of Enterprise Application Architecture", 321127420, 2002, Binding.T, null, null));
        expected.add(new Book(3, "Test Driven Development: By Example", 321146530, 2002, Binding.M, null, null));

        Assertions.assertEquals(expected,actual);

    }
}
