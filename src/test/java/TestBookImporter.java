

import ap.author.Author;
import ap.importer.AuthorImporter;
import ap.book.Binding;
import ap.book.Book;
import ap.importer.BookImporter;
import ap.category.Category;
import ap.importer.CategoryImporter;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class TestBookImporter {

//    @Before
//    public void importCategoriesAndAuthors() throws IOException {
//        AuthorImporter.importAuthors("testAuthors.csv");
//        CategoryImporter.importCAtegories("testCategories.csv");
//    }
//
//    @Test
//    public void importBooksTest() throws IOException {
//        List<Book> listActual = BookImporter.importBooks("testBooks.csv");
//        List<Book> listExpected = new ArrayList<Book>();
//
//        List<Author> authorListBook1 = new ArrayList<Author>();
//        authorListBook1.add(new Author(1, "Robert C.", "Martin", 32));
//        listExpected.add(new Book(1, "Clean Code", 132350882, 2008, Binding.T, authorListBook1, new Category(3, "Techniki programowania", 2)));
//
//        List<Author> authorListBook2 = new ArrayList<Author>();
//        authorListBook2.add(new Author(4, "Joshua", "Bloch", 34));
//        authorListBook2.add(new Author(5, "Eric", "Freeman", 41));
//        authorListBook2.add(new Author(6, "Bert", "Bates", 20));
//        authorListBook2.add(new Author(7, "Kathy", "Sierra", 67));
//        listExpected.add(new Book(5, "Head First Design Patterns", 596007124, 2004, Binding.M, authorListBook2, new Category(2, "Wzorce projektowe", 1)));
//
//        Assertions.assertEquals(listActual, listExpected);
//    }
//
//    @Test(expected = FileNotFoundException.class)
//    public void importBooksTestWrongFileDirectory() throws IOException {
//        BookImporter.importBooks("testdsfsdfBooks.csv");
//    }
}
